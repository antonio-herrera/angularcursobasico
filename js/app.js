var miapp = angular.module('gastosApp', ['ngRoute']);

miapp.controller('homeViewController', ['$scope', function($scope){

   $scope.miappTitulo = "Control de gastos"; 
   
}]);


miapp.config(function($routeProvider) {
   
   $routeProvider
      .when('/',{
         templateUrl: 'vistas/gastos.html',
         controller: 'expensesViewController'
      })
      .when('/gastos',{
         templateUrl: 'vistas/gastos.html',
         controller: 'expensesViewController'
      })
      .when('/gastos/nuevo',{
         templateUrl: 'vistas/formulario.html',
         controller: 'expenseViewController'
      })
      .when('/gastos/edicion/:id',{
         templateUrl: 'vistas/formulario.html',
         controller: 'expenseViewController'
      })
      .otherwise({
         redirectTo: '/'
      });

});

miapp.controller('expensesViewController', ['$scope', function($scope){

   $scope.gastos = [
      {descripcion: 'comida',  cantidad: 10, fecha: '2015-10-01'},
      {descripcion: 'voletos', cantidad: 11, fecha: '2015-10-02'},
      {descripcion: 'comida',  cantidad: 12, fecha: '2015-10-03'},
      {descripcion: 'credito', cantidad: 13, fecha: '2015-10-04'}
   ];
   
}]);

miapp.controller('expenseViewController', ['$scope','$routeParams', function($scope,$routeParams) {
   $scope.algunTexto = "TExto de prueba"
}]);